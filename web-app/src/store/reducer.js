import {
    CREATE_POST_FAILURE,
    CREATE_POST_REQUEST,
    CREATE_POST_SUCCESS,
    FETCH_POSTS_FAILURE,
    FETCH_POSTS_REQUEST,
    FETCH_POSTS_SUCCESS
} from "./actions/actions";

const initialState = {
    loading: false,
    posts: [],
    error: null,
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_REQUEST:
            return {...state, loading: true};
        case FETCH_POSTS_SUCCESS:
            return {...state, loading: false, posts: action.payload};
        case FETCH_POSTS_FAILURE:
            return {...state, loading: false, error: action.payload};
        case CREATE_POST_REQUEST:
            return {...state, loading: true};
        case CREATE_POST_SUCCESS:
            return {...state, loading: false, posts: [...state.posts, action.payload]};
        case CREATE_POST_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};

export default reducer;