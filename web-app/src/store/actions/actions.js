import axiosApi from "../../axiosApi";
import {apiURL} from "../../constants";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, payload: posts});
export const fetchPostsFailure = err => ({type: FETCH_POSTS_FAILURE, payload: err});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = post => ({type: CREATE_POST_SUCCESS, payload: post});
export const createPostFailure = err => ({type: CREATE_POST_FAILURE, payload: err});


export const fetchPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axiosApi.get(`${apiURL}/posts`);
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure());
        }
    };
};

export const createPost = productData => {
    return async dispatch => {
        try {
            dispatch(createPostRequest());

            const response = await axiosApi.post(`${apiURL}/posts`, productData);
            dispatch(createPostSuccess(response.data));
        } catch (e) {
            dispatch(createPostFailure());
            throw e;
        }
    };
};