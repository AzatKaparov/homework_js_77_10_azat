import React from 'react';
import {Button, Form, Modal} from "react-bootstrap";

const MyModal = ({show, close, options, fileChange, optionsChange, onFormSubmit}) => {
    return (
        <Modal show={show} onHide={close}>
            <Modal.Header closeButton>
                <Modal.Title>Add new post!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={onFormSubmit}>
                    <Form.Group className="mb-3">
                        <Form.Label>Username</Form.Label>
                        <Form.Control onChange={optionsChange} value={options.author} type="text" name="author" placeholder="Username..." />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Message</Form.Label>
                        <Form.Control onChange={optionsChange} value={options.message} required={true} as="textarea" type="text" name="message" placeholder="Message..." />
                    </Form.Group>

                    <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Image</Form.Label>
                        <Form.Control onChange={fileChange} name="image" type="file" />
                    </Form.Group>
                    
                    <Button onClick={close} variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </Modal.Body>
        </Modal>
    );
};

export default MyModal;