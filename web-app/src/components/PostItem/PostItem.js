import React from 'react';
import {Card} from "react-bootstrap";
import {apiURL} from "../../constants";

const PostItem = ({author, message, image}) => {
    return (
        <Card className="mb-4">
            <Card.Header>{author ? author : "Anonymous"}</Card.Header>
            <Card.Body>
                <Card.Text>
                    {message}
                </Card.Text>
                {image
                    ? <Card.Img style={{maxHeight: 200, width: 'auto'}} src={`${apiURL}/uploads/${image}`} />
                    : null
                }
            </Card.Body>
        </Card>
    );
};

export default PostItem;