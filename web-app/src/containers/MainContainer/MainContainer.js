import React, {useEffect, useState} from 'react';
import MyModal from "../../components/UI/Modal/MyModal";
import {Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {createPost, fetchPosts} from "../../store/actions/actions";
import PostItem from "../../components/PostItem/PostItem";
import Preloader from "../../components/UI/Preloader/Preloader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";

const MainContainer = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts);
    const loading = useSelector(state => state.loading);
    const [showModal, setShowModal] = useState(null);
    const [options, setOptions] = useState({
        message: "",
        author: "",
        image: "",
    });

    const handleShowModal = () => setShowModal(true);
    const handleCloseModal = () => setShowModal(false);

    const onOptionsChange = e => {
        const name = e.target.name;
        const value = e.target.value;

        setOptions(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setOptions(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onFormSubmit = async postData => {
        await dispatch(createPost(postData));
    }

    const submitFormHandler  = e => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(options).forEach(key => {
            formData.append(key, options[key]);
        })
        onFormSubmit(formData);
    }

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    console.log(posts);

    return (
        <>
            <Preloader show={loading}/>
            <Backdrop show={loading}/>
            <MyModal
                show={showModal}
                close={handleCloseModal}
                options={options}
                fileChange={onFileChange}
                optionsChange={onOptionsChange}
                onFormSubmit={submitFormHandler}
            />
            <div className="MainContainer container p-3">
            <Button onClick={handleShowModal}>Add post</Button>
            <div className="posts my-4">
                {posts.map(post => (
                    <PostItem
                        key={post.id}
                        image={post.image}
                        message={post.message}
                        author={post.author}
                    />
                ))}
            </div>
        </div>
        </>
    );
};

export default MainContainer;