const express = require("express");
const multer  = require('multer');
const path = require('path');
const config = require('../config');
const router = express.Router();
const {nanoid} = require('nanoid');
const fileDb = require('../fs/mainFS');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

router.get(`/`, (req, res) => {
    const messages = fileDb.getItems();
    res.send(messages);
});

const upload = multer({storage});

router.post('/', upload.single('image'), (req, res) => {
    const post = req.body;
    if (!post.message || post.message === "" || post.message === null || post.message === undefined ) {
        return res.status(400).send({error: "You have to to add some message!"});
    } else {
        const reqPost = {
            message: req.body.message,
            author: req.body.author,
        };

        if (req.file) {
            reqPost.image = req.file.filename;
        }

        const newPost = fileDb.addItem(reqPost);

        res.send(newPost);
    }
});


module.exports = router;