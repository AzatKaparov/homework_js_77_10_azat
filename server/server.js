const express = require('express');
const cors = require('cors');
const fileDb = require('./fs/mainFS');
const app = express();
const posts = require('./app/posts');
const port = 4321;

fileDb.init();
app.use(express.json());
app.use(cors());
app.use('/posts', posts);
app.use(express.static('public'));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});